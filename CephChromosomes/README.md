[TOC]

# Chromosomal scale synteny analysis of cephalopod genomes

## Input files
* **Mutual best hits**:
A tablar file of mutual best hits of _B. floridae_ against _E. scolopes_ (Eup or Esc), _D. pealeii_ (Dpe), _O. bimaculoides_ (Obi or Oct), _M. yessoensis_ was obtained using BLASTP and filtering evalues by 1e<sup>-2</sup>. The resulting file contains genes of _B. floridae_ and their hits in each of the other species respectively, forming a core set of 6,821 genes. BFL genes were manually annotated with their corresponding BLG assignments, as described in [Simakov et al. 2020] (https://www.nature.com/articles/s41559-020-1156-z). In addition, BLGA was split manually into BLGA1 (orthologs that correspond to _M. yessoensis_ chromosome Pye5) and BLGA2 (orthologs that correspond to _M. yessoensis_ chromosome Pye16). The file containing the core set of genes is provided here: [sample\_files/bfl-cent.squid.mbh.clus-2renamed.sorted.lab] (https://bitbucket.org/viemet/public/src/master/CephChromosomes/sample_files/bfl-cent.squid.mbh.clus-2renamed.sorted.lab). 
Only genes with hits in each of the other species were retained.  
Additional species can be added with the script [add\_species\_bfl-cent\_file\_clean.py] (https://bitbucket.org/viemet/public/src/master/CephChromosomes/scripts/add_species_bfl-cent_file_clean.py). The format of the input file to add a species should be (reciprocal blastp hits filtered by evalues at least 1e<sup>-2</sup>):  
 
| | |
:--|:--|
BFL\_gene\_id|new\_species\_gene_id  
An example file with orthologs between _Nematostella vectensis_ and _B. floridae_ can be found here: [sample\_files/bfl-nve.mbh] (shttps://bitbucket.org/viemet/public/src/master/CephChromosomes/sample_files/bfl-nve.mbh).

* **Msynt files**: Files with gene id, gene locations, BLG correspondence and BLG color in the format:
 
| | | | | | |
:--|:--|:--|:--|:--|:--|:--|:--|
|Chromosome| Gene_id |start | stop | direction (1 or 0) | color |

BLG colors were defined manually for plotting purposes. They have to match the BLG assignment in the core set gene file, e.g. BLGN has the color "#BB8714".

An example msynt file for _B. floridae_ and _D. pealeii_ is provided in [sample\_files] (https://bitbucket.org/viemet/public/src/master/CephChromosomes/sample_files/). An example file for all BLGs, their corresponding _B. floridae_ genes and color assignments is provided here: [sample\_files/BLG.col.BLGArenamed.sorted.squidmbh.msynt] (https://bitbucket.org/viemet/public/src/master/CephChromosomes/sample_files/BLG.col.BLGArenamed.sorted.squidmbh.msynt). Start and stop are recomputed for hypothetical BLGs, starting with the first gene in a given BLG.

## Chromosome visualization
The script [drawCLGContrib2.4_2.pl] (https://bitbucket.org/viemet/public/src/master/CephChromosomes/scripts/drawCLGContrib2.4_2.pl) was modified from [Simakov et al. 2020] (https://www.nature.com/articles/s41559-020-1156-z) for chromosome visualization in cephalopods. It has different plotting options that were used in the paper. 

### Plot BLGs and all chromosomes of one species
To visualize the distribution of BLGs on chromosomes of one species, run 

```
perl drawCLGContrib2.4_2.pl BLG.col.BLGArenamed.squidmbh.msynt:,algcolor=BLG.col.BLGArenamed.squidmbh.msynt:type=alg,file=bfl-cent.squid.mbh.clus-2renamed.lab,width=40 dpe.alga.col.squidmbh.msynt:algcolor=BLG.col.BLGArenamed.squidmbh.msynt:type=alg,file=bfl-cent.squid.mbh.clus-2renamed.lab,width=40,maxbreak=50000000 > dory_test.svg
```

### Plot specific BLG and its correspondance on chromosomes between species (Fig. 2 c,d)

To plot specific BLGs, we need some additional input files:

* **specific BLG.ordered.msynt file** for specific BLG, with the order of genes in a given BLG corresponding to the order of genes on the chromosomes of first species to be plotted
* **sel file**: chromosomes to be plotted, one chromosome per line
* **algcolor**: file with BLG color assignments (see above msynt files [sample\_files/BLG.col.BLGArenamed.sorted.squidmbh.msynt] (https://bitbucket.org/viemet/public/src/master/CephChromosomes/sample_files/BLG.col.BLGArenamed.sorted.squidmbh.msynt))
* **mutual best hits** (.lab file, see above) - this can also be a file with additonal species added

The msynt file with ordered genes corresponding to a specific BLGs and the sel files can be computed with the script ![scripts/order\_genes\_on\_BLG\_clean.py] (https://bitbucket.org/viemet/public/src/master/CephChromosomes/scripts/order_genes_on_BLG_clean.py). 
It will ask for a specific BLG as input e.g. BLGD:
In a first steps, it will order the genes on that BLG by the first species. Then it finds chromosomes that share at least 10 orthologs in a given BLG and saves them in the sel file. The script first compares the BLG to species 1, then the chromosomes of species 1 to species 2 in respect to the BLG etc.


```

perl drawCLGContrib2.4_2.pl BLG.col.BLGArenamed.squidmbh.BLGDmsynt:ordbool=BLGD.sel,algcolor=BLG.col.BLGArenamed.squidmbh.BLGDmsynt,horiz:type=alg,file=bfl-cent.squid.mbh.clus-2renamed.sorted.lab,width=40 dpe.alga.col.squidmbh.msynt:ordbool=dpe_BLGD.sel,algcolor=BLG.col.BLGArenamed.squidmbh.msynt,horiz:type=alg,file=bfl-cent.squid.mbh.clus-2renamed.sorted.lab,width=40,maxbreak=50000000:type=orthomap,connect=BLG.col.BLGArenamed.squidmbh.BLGDmsynt,highlight=BLGD eup.alga.col.squidmbh.msynt:ordbool=esc_BLGD.sel,algcolor=BLG.col.BLGArenamed.squidmbh.msynt,horiz:type=alg,file=bfl-cent.squid.mbh.clus-2renamed.sorted.lab,width=40,maxbreak=50000000:type=orthomap,connect=dpe.alga.col.squidmbh.msynt,highlight=BLGD oct.alga.col.squidmbh.msynt:ordbool=obi_BLGD.sel,algcolor=BLG.col.BLGArenamed.squidmbh.msynt,horiz:type=alg,file=bfl-cent.squid.mbh.clus-2renamed.sorted.lab,width=40,maxbreak=50000000:type=orthomap,connect=eup.alga.col.squidmbh.msynt,highlight=BLGD > BLGD_test2.svg
```
Usage:
start with the BLG, then the top species, continue the same command for each species that should be plotted

perl drawCLGContrib2.4_2.pl [BLG msynt file]:ordbool=[chromosomes\_to\_plot.sel],algcolor=[msynt file with BLG colors],horiz(plot horizontally or vertically):type=alg,file=[file with othology relationships],width=40 
[first species to plot msynt]:ordbool=[chromosomes\_to\_plot.sel],algcolor=[msynt file with BLG colors],horiz:type=alg,file=[file with othology relationships],width=40,maxbreak=[maximum spacing between the closest pair of genes]:type=orthomap(to connect different chromosomes),connect=[previous species to connect, eg msynt file with BLGs],highlight=[BLG to highlight, all other orthologues are in grey]



## Dotplots and fisher test blobplots
Dependencies:  
library(hrbrthemes)  
library(gtools)  
library(pheatmap)  
library(ggplot2)  
library(extrafont)  

Dotplots and fisher plots were produced with the R scipt [scripts/squid\_dotplots\_clean.R] (https://bitbucket.org/viemet/public/src/master/CephChromosomes/scripts/squid_dotplots_clean.R). If you want to run the script, change the input folder in the script itself. It can be run with the Esc and Dpe msynt files provided. Chromosomes were ordered by hand as exemplified in the script for _E. scolopes_.


## Ceph LGs

CephLGs were explored in different ways. An [R script]  (https://bitbucket.org/viemet/public/src/master/CephChromosomes/scripts/count_significant_blgs-chroms.R) was used to extract the significant blgs per chromosomes, that were first calculated with the fisher test in [scripts/squid\_dotplots\_clean.R] (https://bitbucket.org/viemet/public/src/master/CephChromosomes/scripts/squid_dotplots_clean.R). Interactions between chromosomes between _D. pealeii_ and _O. bimaculoides_ were plotted in graphviz;
the input file was based on significant contributions of BLGs for each Dpe and Obi chromosome (coloring of each chromosome, obtained by running the dotplot script between BLG and Dpe or Obi), the significant interactions betwee the ceph chromosomes (connection between chromosomes, obtained by running the dotplot script between Dpe and Obi) and the number of significantly shared chromosomes on connected chromosomes (thinckness of lines). The input file to graphviz was computed with [scripts/prepare_graphviz2.py] (https://bitbucket.org/viemet/public/src/master/CephChromosomes/scripts/prepare_graphviz2_clean.py). Instead of the core set of genes, all orthologs between species were used (dpe.all.msynt, obi.all.msynt,esc.all.msynt). The script will outpt a .dot file, which was then used to plot a network of cephalopod chromosomes using the command: ```
neato -Tpdf cephlg.dot > cephlg.pdf```

In addition, CephLGs was inferred via counting of orthologous chromosomes in _D. pealeii_ and _O. bimaculoides_ with the least number of BLG combinations.
The script was run several times and summarized by hand in a table. Results were visually compared to the cephlg graph.


