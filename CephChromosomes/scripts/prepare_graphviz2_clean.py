#for the graphs file we need to calculate these things:
#we need a dictionary that gives us the color for each blg

#for the graph we need:
#1st: the significant contribution of BLGs for each Obi or Dory chromosome (to plot the chromosomes)
#2nd: significant interactions between ceph chromosomes
#3rd: the interaction value (number of shared genes) for each chromosome for the significant chromosomes

from collections import defaultdict

################################################################################################################
###############this part is only important to get a dictionary that gives us the colors for each blg############
alg_list=[]
dpe_col_dict={}
dory_dict={}
col_dict={}
with open("bfl-cent.squid.mbh.clus-2renamed.lab") as blg:
    for line in blg.readlines():
        line = line.rstrip("\n")
        columns = line.split("\t")
        if columns[1] not in alg_list:
            alg_list.append(columns[1])
        if columns[1] not in dory_dict:
            dory_dict[columns[1]]=columns[4]

with open("dpe.alga.col.squidmbh.msynt") as dpe:
    for line in dpe.readlines():
        line = line.rstrip("\n")
        columns = line.split("\t")
        columns[5]=columns[5].replace("\"","")
        if columns[1] not in dpe_col_dict:
            dpe_col_dict[columns[1]]=columns[5]

for i in alg_list:
    col_dict[i]=dpe_col_dict[dory_dict[i]]



###############################################################################################################################
##########now we get only the signficant enrichment of each blg for each of the chromosomes ############
##########and calculate the percentage for each connection#######

obi_blg_dict=defaultdict(list) #this dictionary of lists gives us which blgs are significantly enriched on which obi chrom


#this comes from my R code and is simply a transformed table from the fisher table (pairwise-Octopus-BLG_reordered.resd_fisher.csv,pairwise-Doryteuthis-BLG_reordered.resd_fisher.csv) where I counted the significant blgs per chromosome
#script is in count_significant_blgs-chroms.R in the Doryteuthis_mbh folder
with open("obi_blgs.txt") as sig_int:
    for line in sig_int.readlines()[1:]:
        line = line.replace("\"","")
        columns = line.rstrip("\n").split("\t")
        print(line)
        obi_chrom=columns[1]
        sig_obi=columns[2]
        sig_obi=sig_obi[: -1] #removes the trailing comma
        for sig_blg in sig_obi.rstrip("\t").split(","):
            print(sig_blg)
            obi_blg_dict[obi_chrom].append(sig_blg)

print(obi_blg_dict)

##and the same for doryteuthis
dory_blg_dict = defaultdict(list)

with open("dory_blgs.txt") as sig_int:
    for line in sig_int.readlines()[1:]:
        line = line.replace("\"","")
        columns = line.rstrip("\n").split("\t")
        print(line)
        dory_chrom=columns[1]
        sig_dory=columns[2]
        sig_dory=sig_dory[: -1] #removes the trailing comma
        for sig_blg in sig_dory.rstrip("\t").split(","):
            print(sig_blg)
            dory_blg_dict[dory_chrom].append(sig_blg)

print(dory_blg_dict)


#to get the number of genes in each of the blgs per chromosome we can use the bfl-cent file
#we also need the msynt file because that one has infos about the chromosomes

#make a dictonary with the genes for each obi chrom
obi_orthos=defaultdict(list)
obi_chroms=[] #make a list of chroms to loop through

with open("obi.all.msynt") as obi_synt:
    for line in obi_synt.readlines():
        columns = line.rstrip("\n").split("\t")
        obi_orthos[columns[0]].append(columns[1])
        if columns[0] not in obi_chroms:
            if columns[0].startswith("Obi"):
                obi_chroms.append(columns[0])

obi_chroms.sort()

#make a dictonary with the genes for each dory chrom
dory_orthos=defaultdict(list)
dory_chroms=[] #make a list of chroms to loop through

with open("dpe.all.msynt") as dory_synt:
    for line in dory_synt.readlines():
        columns = line.rstrip("\n").split("\t")
        dory_orthos[columns[0]].append(columns[1])
        if columns[0] not in dory_chroms:
            if columns[0].startswith("Dpe"):
                dory_chroms.append(columns[0])
dory_chroms.sort()

#############################count orthologues#############################
#now we can count the number of orthos for the blgs on each chromosome###
obi_ortho_count={}
blg_obi_orthogenes=defaultdict(list)

for chrom in obi_chroms:
    #print(chrom)
    #print(obi_blg_dict[chrom])
    for blg_target in obi_blg_dict[chrom]:
        blg_count = 0
        with open("bfl-cent.squid.mbh.clus-2renamed.lab") as orthos:
            for line in orthos.readlines():
                line = line.replace("\"", "")
                columns = line.rstrip("\n").split("\t")
                blg=columns[1]
                obi_ortho=columns[6]
                if blg==blg_target:
                    #print(blg_target)
                    if obi_ortho in obi_orthos[chrom]:
                        #print(obi_ortho)
                        blg_count=blg_count+1
        #print(blg_count)
        blg_obi_orthogenes[chrom].append(blg_count)
#print(obi_blg_dict)
#print(blg_obi_orthogenes)
#print(obi_chroms)

dory_ortho_count={}
blg_dory_orthogenes=defaultdict(list)

for chrom in dory_chroms:
    #print(chrom)
    #print(dory_blg_dict[chrom])
    for blg_target in dory_blg_dict[chrom]:
        blg_count = 0
        with open("bfl-cent.squid.mbh.clus-2renamed.lab") as orthos:
            for line in orthos.readlines():
                line = line.replace("\"", "")
                columns = line.rstrip("\n").split("\t")
                blg=columns[1]
                dory_ortho=columns[4]
                if blg==blg_target:
                    #print(blg_target)
                    if dory_ortho in dory_orthos[chrom]:
                        #print(dory_ortho)
                        blg_count=blg_count+1
        #print(blg_count)
        blg_dory_orthogenes[chrom].append(blg_count)
#print(dory_blg_dict)
#print(blg_dory_orthogenes)
#print(dory_chroms)

#####lets write the first part of the output file, the chromosomes####
graph_file=open("cephlg.dot","w")

blg_per_chrom_dict={}
colors=""
print(col_dict)
graph_file.write("graph\t{\n")
for chrom in obi_chroms:
    print(chrom)
    print(blg_obi_orthogenes[chrom])
    if sum(blg_obi_orthogenes[chrom])==0:
        graph_file.write("%s\t[\tshape\t=\tcircle\tstyle\t=\twedged\tfontsize\t=\t10\t]\n" % (chrom))
    else:
        colors=""
        total = sum(blg_obi_orthogenes[chrom])
        #print("fillcolor=\"%s:" % (col_dict[blg]))
        print("%s\t[\tshape\t=\tcircle\tstyle\t=\twedged\t" % (chrom))
        graph_file.write("%s\t[\tshape\t=\tcircle\tstyle\t=\twedged\tfontsize\t=\t10\t" % (chrom))
        count0 = []
        for count in blg_obi_orthogenes[chrom]:
            if count in count0:
                print("same number in list")
                ind = ind+1
                print(ind)
                blg = obi_blg_dict[chrom][ind]
                percent = count / total
                colors = colors + (("%s;%s:") % (col_dict[blg], percent))
            else:
                ind=blg_obi_orthogenes[chrom].index(count)#get index of the count to get blg name
                blg=obi_blg_dict[chrom][ind]
                percent = count / total
                colors = colors + (("%s;%s:") % (col_dict[blg], percent))
                count0.append(count)
        print("fillcolor=\"%s:" % (colors))
        graph_file.write("fillcolor=\"%s" % (colors))
        print("black\"]\n")
        graph_file.write("black\"]\n")


blg_per_chrom_dict={}
colors=""
print(col_dict)

for chrom in dory_chroms:
    print(chrom)
    print(blg_dory_orthogenes[chrom])
    if sum(blg_dory_orthogenes[chrom])==0:
        graph_file.write("%s\t[\tshape\t=\tcircle\tstyle\t=\twedged\tfontsize\t=\t10\t]\n" % (chrom))
    else:
        colors=""
        total = sum(blg_dory_orthogenes[chrom])
        #print("fillcolor=\"%s:" % (col_dict[blg]))
        print("%s\t[\tshape\t=\tcircle\tstyle\t=\twedged\t" % (chrom))
        graph_file.write("%s\t[\tshape\t=\tcircle\tstyle\t=\twedged\tfontsize\t=\t10\t" % (chrom))
        count0 = []
        for count in blg_dory_orthogenes[chrom]:
            if count in count0:
                ind = ind+1  # get index of the count to get blg name
                blg = dory_blg_dict[chrom][ind]
                percent = count / total
                colors = colors + (("%s;%s:") % (col_dict[blg], percent))
                print(chrom)
                print(dory_blg_dict[chrom])
                print(blg)
                print(col_dict[blg])
                print(ind)
            else:
                ind=blg_dory_orthogenes[chrom].index(count)#get index of the count to get blg name
                blg=dory_blg_dict[chrom][ind]
                percent = count / total
                colors = colors + (("%s;%s:") % (col_dict[blg], percent))
                print(chrom)
                print(dory_blg_dict[chrom])
                print(blg)
                print(col_dict[blg])
                print(ind)
                count0.append(count)
        print("fillcolor=\"%s" % (colors))
        graph_file.write("fillcolor=\"%s" % (colors))
        print("black\"]\n")
        graph_file.write("black\"]\n")



##now we just need the connections; these are the shared genes between ceph chromosomes, but only of significant BLGs
#we just need a count of shared genes for significant chromosomes

#we only need one connection for each link this:
#Dpe01 -- Obi01
#Dpe01 -- Obi04

#we can use the table I created in R dory_obi_cephlgs
#we need a dictionary of the number of shared genes for each chromosome combination
#orthologues are in bfl-cent.squid.mbh.clus-2renamed.lab

ceph_orthos={} #dictionaries with orthologues between dory and obi

#we also have a dictionar which orthologues are in which blg

with open("bfl-cent.squid.mbh.clus-2renamed.lab") as orthos:
    for line in orthos.readlines():
        line = line.replace("\"", "")
        columns = line.rstrip("\n").split("\t")
        ceph_orthos[columns[4]]=columns[6]



#dory_orthos gives us the orthologues on each dory chromosome

print(ceph_orthos)
missing_orthos=[]
with open("dory_obi_cephlgs.txt") as ceph_lgs:
    for line in ceph_lgs.readlines()[1:]:
        line = line.replace("\"", "")
        columns = line.rstrip("\n").split("\t")
        #we need column 0 (Dpe chroms) and column 2 (connected obi chroms)
        for connection in columns[2].split(","):
            count = 0
            if connection.startswith("Obi"):
                for ortholog in dory_orthos[columns[0]]:
                    try:
                        if ceph_orthos[ortholog] in obi_orthos[connection]:
                            count=count+1
                    except KeyError:
                        missing_orthos.append(ortholog)
                print("\t%s\t--\t%s\t[\tweight\t=\t%s\tpenwidth\t=\t%s]" % (columns[0],connection,count,count/10))
                graph_file.write("\t%s\t--\t%s\t[\tweight\t=\t%s\tpenwidth\t=\t%s]\t\t\n"% (columns[0],connection,count,count/10))
            else:
                continue

#print(missing_orthos)

graph_file.write("\t}\t\n\n")
graph_file.close

#print(col_dict)