#this script adds more species to the bfl syntfile
#(file with core set of orthologues between B. floridae and E. scolopes, D. pealeii, O. bimaculoides, M. yessoensis)




print("which species do you want to add? please give me file and file path")
new_species=input()

ortho_dict={}
with open(new_species) as orthos:
    for line in orthos.readlines():
        line=line.rstrip("\n")
        columns=line.split("\t")
        ortho_dict[columns[0]]=columns[1]

#print(ortho_dict)
print("Please provide file path and name to your core gene set")
core_genes=input()
print("Please provide file path and name for your output file; I will create a new file with all entries from the core gene set, adding the new species and removing any genes that are not present in the new species")
new_file=input()
with open(core_genes) as blg:
    new_blg=open("new_file","w")
    for line in blg.readlines():
        line=line.rstrip("\n")
        columns=line.split("\t")
        if columns[2] in ortho_dict:
            print(line,ortho_dict[columns[2]])
            new_blg.write("%s\t%s\n"%(line,ortho_dict[columns[2]]))
