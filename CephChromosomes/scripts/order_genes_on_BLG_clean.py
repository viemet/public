#this script makes sel files and orders the genes on the blgs for plotting;
#we will ask for a BLG as input and then get the corresponding files as output;
# bfl-cent.squid.mbh.clus-2renamed.lab needs to be ordered by doryteuthis chromosomes;
# the dory chromosomes need to be ordered by number of genes on the chromosomes that correspond to that blg
# then the genes in bfl cent have to be ordered on the blg by the order of chromosomes (large to small)

#we need: dictionary with number of genes for BLG on Dory chroms (only if chroms exist)
#list that is sorted by highest to lowest number
#reordered bfl cent file by locations in msynt file - dory genes are already ordered in this file

from collections import defaultdict

print("Which BLG do you want to plot?")
BLG=input()
print(BLG)
dory_genes=[]
esc_genes=[]
obi_genes=[]
dory_blg_genes=defaultdict(list)
dory_chroms=[]
#we also need a dictionary of dory - esc orthologues and of esc - obi orthologues
dory_esc_orthos={}
esc_obi_orthos={}

with open("bfl-cent.squid.mbh.clus-2.lab") as bfl_cent:
    for line in bfl_cent.readlines():
        columns=line.rstrip("\n").split("\t")
        blgs=columns[1]
        d_genes=columns[4]
        e_genes=columns[5]
        o_genes=columns[6]
        if BLG==blgs:
            dory_genes.append(d_genes)
            esc_genes.append(e_genes)
            obi_genes.append(o_genes)
            dory_esc_orthos[d_genes]=e_genes
            esc_obi_orthos[e_genes]=o_genes

with open("dpe.alga.col.squidmbh.msynt") as msynt:
    for line in msynt.readlines():
        columns=line.rstrip("\n").split("\t")
        chroms=columns[0]
        genes=columns[1]
        if genes in dory_genes:
            dory_blg_genes[chroms].append(genes)
            if chroms not in dory_chroms:
                dory_chroms.append(chroms)


esc_blg_genes=defaultdict(list)
esc_chroms=[]

with open("eup.alga.col.squidmbh.msynt") as msynt:
    for line in msynt.readlines():
        columns=line.rstrip("\n").split("\t")
        chroms=columns[0]
        genes=columns[1]
        if genes in esc_genes:
            esc_blg_genes[chroms].append(genes)
            if chroms not in esc_chroms:
                esc_chroms.append(chroms)

obi_blg_genes=defaultdict(list)
obi_chroms=[]

with open("oct.alga.col.squidmbh.msynt") as msynt:
    for line in msynt.readlines():
        columns=line.rstrip("\n").split("\t")
        chroms=columns[0]
        genes=columns[1]
        if genes in obi_genes:
            obi_blg_genes[chroms].append(genes)
            if chroms not in obi_chroms:
                obi_chroms.append(chroms)



size_list={}
index=0

#we only want to keep chromosomes with at least 10 genes belonging to BLGM
for chrom in dory_chroms:
    if len(dory_blg_genes[chrom])<10:
        dory_blg_genes.pop(chrom)

sorted_dory_chroms=(sorted(dory_blg_genes, key=lambda k: len(dory_blg_genes[k]), reverse=True)) #sort dictionaries by lenth

for chrom in esc_chroms:
    if len(esc_blg_genes[chrom])<10:
        esc_blg_genes.pop(chrom)

sorted_esc_chroms=(sorted(esc_blg_genes, key=lambda k: len(esc_blg_genes[k]), reverse=True)) #sort dictionaries by lenth


for chrom in obi_chroms:
     if len(obi_blg_genes[chrom])<10:
         obi_blg_genes.pop(chrom)
     if len(obi_blg_genes[chrom])<10:
         print(BLG)
         print(chrom)
         print(obi_blg_genes[chrom])

sorted_obi_chroms=(sorted(obi_blg_genes, key=lambda k: len(obi_blg_genes[k]), reverse=True)) #sort dictionaries by lenth
print(sorted_obi_chroms)

#because we want the esc chroms to be sorted by their correspondence to obi chroms, we need to find out how many
#esc genes of each of our esc chroms are on the obi chroms we are plotting
dory_chrom_escorthos=defaultdict(list)
for chrom in sorted_dory_chroms:
    for gene in dory_blg_genes[chrom]:
         dory_chrom_escorthos[chrom].append(dory_esc_orthos[gene])

esc_chrom_obiorthos=defaultdict(list)
for chrom in sorted_esc_chroms:
    for gene in esc_blg_genes[chrom]:
         esc_chrom_obiorthos[chrom].append(esc_obi_orthos[gene])




#now we have all the esc orthologues on each of the dory genes; we just need to find which of our esc
#chroms has most of the corresponding orthologues

#print(dory_chrom_escorthos)
l_shared_chrom=()
corr_chroms=defaultdict(list)
no_shared={}
for dpe_chrom in sorted_dory_chroms:
    shared1=0
    for esc_chrom in sorted_esc_chroms:
        #print(esc_chrom)
        #print(dpe_chrom)
        #print(dory_chrom_escorthos[dpe_chrom])
        #print(list(set(dory_chrom_escorthos[dpe_chrom]) & set(esc_blg_genes[esc_chrom])))
        shared2=len(list(set(dory_chrom_escorthos[dpe_chrom]) & set(esc_blg_genes[esc_chrom])))
        if int(shared2) >= 10:
            print("shared2 longer than 10")
            shared1 = shared2
            l_shared_chrom = esc_chrom
            print(esc_chrom)
            print(dpe_chrom)
            print(shared2)
                # print(dpe_chrom)
                # print(l_shared_chrom)
        #if dpe_chrom not in corr_chroms:
            #corr_chroms[dpe_chrom]=l_shared_chrom
        #if dpe_chrom in corr_chroms:
        if l_shared_chrom not in corr_chroms[dpe_chrom]:
            corr_chroms[dpe_chrom].append(l_shared_chrom)
    print(l_shared_chrom)
    no_shared[dpe_chrom] = shared1


    #print(dpe_chrom)
    #print(l_shared_chrom)
    #corr_chroms[dpe_chrom]=l_shared_chrom
    #no_shared[esc_chrom]=shared1
#print(sorted_esc_chroms)
print(corr_chroms)
print(no_shared)



#now we can make the sel file:
with open("dpe_%s.sel"%(BLG),"w")as sel:
    for chrom in sorted_dory_chroms:
        sel.write("%s\n"%chrom)

#now we have some chromosomes that share a lot, while others only share little; we first want the chrom that shares a lot, t
# then the one that shres little for our sel file
#we also need to make a new file for the esc chromosome order; we can use esc chrom used
no_shared=0
esc_chrom_used=[]
with open("esc_%s.sel"%(BLG),"w")as sel:
    for chrom in sorted_dory_chroms:
        if corr_chroms[chrom] not in esc_chrom_used:
            print("Printing corr_chroms chrom")
            print(corr_chroms[chrom])
            for esc_chrom in corr_chroms[chrom]:
                print(esc_chrom)
                if esc_chrom not in esc_chrom_used:
                    esc_chrom_used.append(esc_chrom)
                    sel.write("%s\n" % esc_chrom)
            #esc_chrom_used.append(corr_chroms[chrom])
            #print(corr_chroms[chrom])
        else:
            continue

    #for chrom in sorted_esc_chroms:
    #sel.write("%s\n"%chrom)
#now we use the new chromosome order for euprymna, esc_chrom_used instead of sorted_esc_chros

l_shared_chrom=()
corr_chroms_escobi=defaultdict(list)
no_shared_escobi={}
print(sorted_obi_chroms)
for esc_chrom in set(esc_chrom_used):
    print("printing esc_chrom")
    print(esc_chrom)
    shared1=0
    for obi_chrom in sorted_obi_chroms:
        #print(esc_chrom_obiorthos[esc_chrom])
        #print(esc_blg_genes[esc_chrom])
        shared2=len(list(set(esc_chrom_obiorthos[esc_chrom]) & set(obi_blg_genes[obi_chrom])))

        #shared2=len(list(set(dory_chrom_escorthos[dpe_chrom]) & set(esc_blg_genes[esc_chrom])))


        print(esc_chrom)
        print(obi_chrom)
        print(shared2)
        if int(shared2)>=10:
            print("shared2 longer than shared1")
            shared1=shared2
            l_shared_chrom=obi_chrom
            print(esc_chrom)
            print(obi_chrom)
            print(shared2)
    #print(dpe_chrom)
    #print(l_shared_chrom)
        if l_shared_chrom not in corr_chroms_escobi[esc_chrom]:
            corr_chroms_escobi[esc_chrom].append(l_shared_chrom)
    #print(corr_chroms_escobi)
    print(l_shared_chrom)
    no_shared_escobi[esc_chrom]=shared1
#print(sorted_obi_chroms)
print(corr_chroms_escobi)
#print(no_shared_escobi)

no_shared=0
obi_chrom_used=[]
with open("obi_%s.sel"%(BLG),"w")as sel:
    for chrom in esc_chrom_used:
        for obi_chrom in corr_chroms_escobi[chrom]:
            if obi_chrom not in obi_chrom_used:
                try:
                    obi_chrom_used.append(obi_chrom)
            #print(corr_chroms_escobi[chrom])
                    sel.write("%s\n"%obi_chrom)
                except TypeError:
                    print("Whoopsie")

        else:
            continue
    #for chrom in sorted_esc_chroms:
    #sel.write("%s\n"%chrom)





# print(' '.join(sorted(dory_blg_genes, key=lambda k: len(dory_blg_genes[k]), reverse=True))) #sort dictionaries by lenth
# print(sorted_dory_chroms)
# print(dory_blg_genes)
# for chrom in sorted_dory_chroms:
#     print(chrom)
#     print(len(dory_blg_genes[chrom]))
#





#we want the esc chromosomes to be ordered by their dpe correspondence, not the number of genes shared with blgD
with open("%s.sel"%(BLG),"w")as sel:
    sel.write("%s\n"%BLG)
#and now we just need the bfl cent file to be reordered by the dory genes
#seems like we need to reorder this file: BLG.col.BLGArenamed.squidmbh.msynt not the bfl cent file
#new_cent=open("bfl-cent.squid.mbh.clus-2.%s.lab"%(BLG),"w")
bfl_list=[]
for chrom in sorted_dory_chroms:
    for gene in dory_blg_genes[chrom]:
        with open("bfl-cent.squid.mbh.clus-2.lab") as bfl_cent:
            for line in bfl_cent.readlines():
                line=line.rstrip("\n")
                columns=line.split("\t")
                bfl_gene=columns[2]
                if gene in columns[4]:
                    #print(line)
                    bfl_list.append(bfl_gene)  #the genes should now have the same order as in the dory files
                    #new_cent.write(("%s\n")%line)
                    #print(gene)
                    #print(chrom)
                    #print(dory_blg_genes[chrom])
#new_cent.close()
#we also need to recalculate the locations
location=0
new_blg_cols=open("BLG.col.BLGArenamed.squidmbh.%smsynt" %(BLG),"w")
for gene in bfl_list:
    with open("BLG.col.BLGArenamed.squidmbh.msynt") as blg_cols:
        for line in blg_cols.readlines():
            line=line.rstrip("\n")
            columns=line.split("\t")
            if gene in columns[1]:
                #print(line)
                location=location+1
                new_blg_cols.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\n" %(columns[0],columns[1],location,columns[3],columns[4],columns[5],columns[6] ))
#then we want to append the other genes that belong to BLGM so that the chromome is plotted correctly:

with open("BLG.col.BLGArenamed.squidmbh.msynt") as blg_cols:
    for line in blg_cols.readlines():
        line=line.rstrip("\n")
        columns=line.split("\t")
        if columns[1]==BLG:
            if columns[1] not in bfl_list:
                locations= location+1
                new_blg_cols.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\n" %(columns[0],columns[1],location,columns[3],columns[4],columns[5],columns[6]))


with open("BLG.col.BLGArenamed.squidmbh.msynt") as blg_cols:
    for line in blg_cols.readlines():
        line=line.rstrip("\n")
        columns=line.split("\t")
        if columns[1]==BLG:
            if columns[0]!=BLG:
                locations= location+1
                new_blg_cols.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\n" %(columns[0],columns[1],location,columns[3],columns[4],columns[5],columns[6]))