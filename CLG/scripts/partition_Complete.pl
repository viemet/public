#!/usr/bin/perl -w
use strict;

my $minpart=$ARGV[1];

open(I,"<$ARGV[0]");
my %chr=();
while (<I>) {
 chomp;
 my @tmp = split /\t/;
 if ($tmp[6] eq "NOCLG") { next }
 push @{$chr{$tmp[0]}}, [ (@tmp) ];
}
close I;

for my $scf (keys %chr) {
 my @ar=@{$chr{$scf}};
 my %bor=();
 my $maxpos=-1;
 while ($maxpos>=-1) {
 my $max=0;
 $maxpos=-1;
 for my $x (($minpart-1)..($#ar-$minpart)) {
	 my %left=();
	 my %right=();
	 my $ok=1;
	 for my $y (($x-$minpart+1)..$x) {
		 if (exists $bor{$y}) { $ok=0 }
		 $left{$ar[$y][6]}++;
 	 }
	 for my $y (($x+1)..($x+$minpart)) {
		 if (exists $bor{$y}) { $ok=0 }
                 $right{$ar[$y][6]}++;
         }
	 my $dist=0;
	 for my $y ("A".."Q") {
	  my $clg="CLG$y";
	  my $l=0; if (exists $left{$clg}) { $l=$left{$clg} }
	  my $r=0; if (exists $right{$clg}) { $r=$right{$clg} }
	  $dist+=($l-$r)**2;
	 }
	 if (($dist>$max)&&($ok==1)) { 
		 $max=$dist; $maxpos=$x;
	 }
 }
 if ($maxpos>=0) { $bor{$maxpos}=1 } else { $maxpos=-100 }
 }
 my @s=sort {$a <=> $b} keys %bor;
 if ($#s==-1) { @s=(-1,$#ar) }
 if ($s[0] eq "0") { } else { @s=(-1,@s) }
 if ($s[$#s] eq $#ar) { } else { @s=(@s,$#ar) }
 for my $x (1..$#s) {
  for my $y (($s[$x-1]+1)..($s[$x])) {
	  $ar[$y][0].=".$x";
   print "".join("\t",@{$ar[$y]})."\n";
  }
 }
 $ar[$s[$#s]][0].=".$#s";
 print "".join("\t",@{$ar[$s[$#s]]})."\n";
}

