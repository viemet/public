#!/usr/bin/perl -w
use strict;
use Color::Rgb;

my %clg_col=( 
 "CLGA" => [153,153,153],
 "CLGB" => [51,51,51],
 "CLGC" => [153,15,15],
 "CLGD" => [204,81,81],
 "CLGE" => [255,178,178],
 "CLGF" => [153,84,15],
 "CLGG" => [204,142,81],
 "CLGH" => [255,216,178],
 "CLGI" => [107,153,15],
 "CLGJ" => [163,204,81],
 "CLGK" => [229,255,178],
 "CLGL" => [15,107,153],
 "CLGM" => [81,163,204],
 "CLGN" => [178,229,255],
 "CLGO" => [38,15,153],
 "CLGP" => [102,81,204],
 "CLGQ" => [191,178,255]

);

my %clg_col2=(
 "CLGA" => "#E41A1C",
 "CLGB" => "#8D4C6A",
 "CLGC" => "#377EB8",
 "CLGD" => "#419681",
 "CLGE" => "#4DAF4A",
 "CLGF" => "#727E76",
 "CLGG" => "#984EA3",
 "CLGH" => "#CB6651",
 "CLGI" => "#FF7F00",
 "CLGJ" => "#FFBF19",
 "CLGK" => "#FFFF33",
 "CLGL" => "#D2AA2D",
 "CLGM" => "#A65628",
 "CLGN" => "#CE6B73",
 "CLGO" => "#F781BF",
 "CLGP" => "#C88DAC",
 "CLGQ" => "#999999",
 "CLGR" => "#999999",
 "CLGS" => "#999999"
);


my $rgb=new Color::Rgb(rgb_txt=>'/usr/share/X11/rgb.txt');

open(I,"<$ARGV[0]");
my %fam0=();
my %map=();
while (<I>) {
 chomp;
 my @tmp = split /\t/;
 for my $x (@tmp[2..$#tmp]) {
  $map{$x}=$tmp[1];
 }
}
close I;

my %fam=();
for my $x (keys %fam0) {
 if (keys(%{$fam0{$x}})==1) { my @ar=keys %{$fam0{$x}}; $fam{$x}=$ar[0] }
}

open(I,"<$ARGV[2]");
while (<I>) {
 chomp;
 my @tmp = split /\t/;
 my @col=(225,225,225);
 my $hex=uc $rgb->rgb2hex(int($col[0]),int($col[1]),int($col[2]),"#");
 my $cm="NOCLG";
 if (exists $map{$tmp[1]}) {
  $hex=$clg_col2{$map{$tmp[1]}};
  $cm=$map{$tmp[1]};
 } 
 @tmp = @tmp[0..4];
 print join("\t",@tmp)."\t\"$hex\"\t$cm\n";
}
close I;

