#!/usr/bin/perl -w
use strict;
use SVG;
use Color::Mix;

sub get_distinct_colors {
  use POSIX 'ceil';

  my $n = shift;
  my $discrete = ceil($n ** (1/3));
  my @vals = map 1 - (($_-1) / $discrete), 1 .. $discrete;
  my @colors;
  my ($r, $g, $b) = (0,0,0);

  for my $i (1 .. $n) {
    push @colors, [@vals[$r,$g,$b]];
    if (++$b == $discrete) {
      if (++$g == $discrete) {
        $r = ($r + 1) % $discrete;
        $g = 0;
      }
      $b = 0;
    }
  }   
  return \@colors;
} 

sub get_distinct_colors2 {
 my @colors=(
#  [ 0.600, 0.060, 0.060 ],
# [ 0.800, 0.320, 0.320],
# [1.000, 0.700, 0.700],
# [ 0.600, 0.330, 0.060],
# [0.800, 0.560, 0.320],
# [1.000, 0.850, 0.700],
# [0.420, 0.600, 0.060],
# [0.640, 0.800, 0.320],
# [0.900, 1.000, 0.700],
# [0.060, 0.420, 0.600],
# [0.320, 0.640, 0.800],
# [0.700, 0.900, 1.000],
# [0.150, 0.060, 0.600],
# [0.400, 0.320, 0.800],
# [0.750, 0.700, 1.000],
# [0.600, 0.600, 0.600],
# [0.200, 0.200, 0.200]
[0.8941176,0.1019608,0.10980392],
[0.5529412,0.2980392,0.41568627],
[0.2156863,0.4941176,0.72156863],
[0.2549020,0.5882353,0.50588235],
[0.3019608,0.6862745,0.29019608],
[0.4470588,0.4941176,0.46274510],
[0.5960784,0.3058824,0.63921569],
[0.7960784,0.4000000,0.31764706],
[1.0000000,0.4980392,0.00000000],
[1.0000000,0.7490196,0.09803922],
[1.0000000,1.0000000,0.20000000],
[0.8235294,0.6666667,0.17647059],
[0.6509804,0.3372549,0.15686275],
[0.8078431,0.4196078,0.45098039],
[0.9686275,0.5058824,0.74901961],
[0.7843137,0.5529412,0.67450980],
[0.6000000,0.6000000,0.60000000],
[0.2000000,0.6000000,0.60000000],
[0.6000000,0.2000000,0.20000000],
[0.2000000,0.2000000,0.20000000]
 );
 return \@colors;
}

 my $scheme2=Color::Mix->new;   
 #my @s2 = $scheme2->analogous('f00000', 20, 20);
 my @s2=@{get_distinct_colors(50)};
 @s2=@{get_distinct_colors2()};


my $wid=20;
my $hei=400;
my $spacer=25;

my $svg = SVG->new(
    width  => 4000,
    height => ($hei+100)*($#ARGV+1)+300,
);


my %b=();
my %bs=();
my $max=0;

my $xloc=25;
my $yloc=50;

my %SEGCOL=();
my %HITSEGPOS=();

for my $com (@ARGV[0..$#ARGV]) {
my @data = split /\,/,$com;
%b=();
%bs=();
$max=0;
plot_chr($data[0],$data[1],$data[2],$data[3],$data[4],$data[5]);
$xloc=25;
$yloc+=500;

if ($data[1] eq "") { next }
my %alg=();
my %alg_co=();
my %pal=();
open(I,"<$data[1]");
while (<I>) {
 chomp;
 my @tmp = split /\t/;
  my ($chr0,$seg0)=($tmp[0],1);
  if ($tmp[0]=~/([^\.]*)\.(.*)/) {
   $chr0=$1;
   $seg0=$2;
  }
  #print STDERR " $chr0 $seg0 -> \n";
  my $t=0;
  my $sid=$tmp[0];
  for my $x (@tmp[1..$#tmp]) {
   my ($id,$co)=$x=~/(.*)\-(\d+)/;
   #if ($co<10) { next }
   $t+=$co;
   push @{$alg{$sid}}, $id;
   push @{$alg_co{$sid}}, $co;
   $pal{$id}{$sid}=1;
  }
  if ($t<5) { next }
  my $shiftx=0;
  for my $x (0..$#{$alg_co{$sid}}) {
   ${$alg_co{$sid}}[$x]=${$alg_co{$sid}}[$x]/$t;
   my $r=${$alg_co{$sid}}[$x];
   my ($chr,$seg)=($alg{$sid}[$x],1);
   if ($alg{$sid}[$x]=~/([^\.]*)\.(.*)/) {
    $chr=$1;
    $seg=$2;
   }
   if (not exists $HITSEGPOS{$chr0}{$seg0}) { next }
   if (not exists $SEGCOL{$chr}{$seg}) { next }
   my $col=$SEGCOL{$chr}{$seg};
   #print STDERR "   $chr $seg $col $r\n";
   $svg->rectangle(
   x=>${$HITSEGPOS{$chr0}{$seg0}}[0]+$shiftx,
   y=>${$HITSEGPOS{$chr0}{$seg0}}[1],
   width=>${$HITSEGPOS{$chr0}{$seg0}}[2]*$r,
   height=>${$HITSEGPOS{$chr0}{$seg0}}[3],
   style=> {
    'fill' => "rgb(".$col.")",
    'stroke'         => 'red',
    'stroke-width'=>'0',
   }
   );
   $shiftx+=${$HITSEGPOS{$chr0}{$seg0}}[2]*$r;
  }
}
close I;





}

print $svg->xmlify;




sub plot_chr {
 my $FILE=$_[0];
 my $colbool="";
 my $ordbool="";
 my %cent=();
 if ($_[1]) { $colbool=$_[1] }
 if ($_[2]) { $ordbool=$_[2] }
 if ($_[3]) { 
  open(I,"<$_[3]");
  print STDERR " $_[3] \n";
  my $ccid=0;
  while (<I>) {
   chomp;
   my @tmp = split /\t/;
    $ccid++;
   #if (exists $cent{$tmp[0]}{1}) { $cent{$tmp[0]}{2}=$tmp[2] } else {
    $cent{$tmp[0]}{$ccid}{1}=$tmp[1];
    $cent{$tmp[0]}{$ccid}{2}=$tmp[2];
   #}
  }
  close I;
 }
 my %segbound=();
 if ($_[4]) {
 open(I,"<$_[4]");
  print STDERR " $_[4] \n";
  while (<I>) {
   chomp;
   my @tmp = split /\t/;
   my ($sid)=$tmp[0]=~/^([^\.]*)/;
   push @{$segbound{$sid}},[($tmp[1],$tmp[2]) ];
  }
  close I;
 }

 my %segbound2=();
 if ($_[5]) {
 open(I,"<$_[5]");
  print STDERR " $_[5] \n";
  while (<I>) {
   chomp;
   my @tmp = split /\t/;
   my ($sid)=$tmp[0]=~/^([^\.]*)/;
   push @{$segbound2{$sid}},[($tmp[1],$tmp[2]) ];
  }
  close I;
 }



my $selpat=".*";
if (!(-e $ordbool)) {
 $selpat=$ordbool;
}

open(I,"<$FILE");
while (<I>) {
 chomp;
 my @tmp = split /\t/;
 my $seg=1;
 my $chr=$tmp[0];
 if ($tmp[0]=~/([^\.]*)\.(.*)/) {
  $chr=$1;
  $seg=$2;
 }
 if ($chr=~/^$selpat/) { } else { next }
 if (not exists $b{$chr}{$seg}{1}) { $b{$chr}{$seg}{1}=$tmp[2] } else { if ($tmp[2]<$b{$chr}{$seg}{1}) { $b{$chr}{$seg}{1}=$tmp[2] } }
 if (not exists $b{$chr}{$seg}{2}) { $b{$chr}{$seg}{2}=$tmp[2] } else { if ($tmp[2]>$b{$chr}{$seg}{2}) { $b{$chr}{$seg}{2}=$tmp[2] } }
 if (not exists $bs{$chr}) { $bs{$chr}=$tmp[2] } else { if ($tmp[2]>$bs{$chr}) { $bs{$chr}=$tmp[2] } }
 if ($tmp[2]>$max) { $max=$tmp[2] }
}
close I;

my @chr=sort { 
 my $alpha=0; 
 my $ac=$a; 
 if ($a=~/^\D{1,}(\d+)/) { $ac=$1; } 
 if ($a=~/^\D{1,}$/) {$alpha=1 }
 if ($a=~/^(\d+)\D{1,}/) { $ac=$1; }
 my $bc=$b; 
 if ($b=~/^\D{1,}(\d+)/) { $bc=$1; } 
 if ($b=~/^(\d+)\D{1,}/) { $bc=$1; }
 if ($b=~/^\D{1,}$/) {  if ($alpha==1) { $alpha=3 } else { $alpha=2 } }
 if ($alpha==0) {  return $ac <=> $bc } else { if ($alpha==1) { return 1<=>0 }; if ($alpha==2) { return 0<=>1 }; if ($alpha==3) { return $ac cmp $bc };  }
} keys %bs;

print STDERR "ordbool = $ordbool ; selpat = $selpat\n";
if (($ordbool eq "")||($ordbool eq $selpat)) { } else {
 %bs=();
 my $upper=10000;
 open(I,"<$ordbool");
 while (<I>) {
  chomp;
  my @tmp = split /\t/;
  $upper--;
  $bs{$tmp[0]}=$upper;
 }
 @chr=reverse sort {$bs{$a} <=> $bs{$b}} keys %bs;
}

my $colid=-1;
for my $x (@chr) {
 #print STDERR "$x ... \n";
 #$svg->text( x=>$xloc-10, y=>$yloc-20,transform=>"rotate(-90,".($xloc-15).",".($yloc-25).") transform(100,0) scale(-1,1)")->cdata("$x");
 $svg->text( x=>$xloc-30, y=>$yloc,transform=>"rotate(-90,".($xloc-15).",".($yloc-25).")")->cdata("$x");
 if (exists $cent{$x}) {
  #print STDERR "$x exists!\n";
  for my $ccid (keys %{$cent{$x}}) {
  my $cury=int $cent{$x}{$ccid}{1}/$max*$hei;
  my $cury2=int $cent{$x}{$ccid}{2}/$max*$hei;
  my $yh=$cury2-$cury+1;
  $svg->rectangle(
   x=>$xloc-$wid/2,
   y=>$yloc+$cury,
   width=>$wid*2,
   height=>$yh,
   style=> {
    'fill' => "grey",
    'stroke'         => "grey",
   }
  );
  }
 }
 if (exists $segbound{$x}) {
  #print STDERR "$x exists!\n";
  my $tmpcol=-1;
  my @tmpcol=("green","magenta");
  for my $xxx (0..$#{$segbound{$x}}) {
  $tmpcol++;
  my $cury=int ${$segbound{$x}}[$xxx][0]/$max*$hei;
  my $cury2=int ${$segbound{$x}}[$xxx][1]/$max*$hei;
  my $yh=$cury2-$cury+1;
  $svg->rectangle(
   x=>$xloc-$wid/2+$wid/6,
   y=>$yloc+$cury,
   width=>$wid/4,
   height=>$yh,
   style=> {
    'fill' => $tmpcol[$tmpcol],
    'stroke'         => "white",
    'fill-opacity' => 0.5,
   }
  );
  if ($tmpcol==1) { $tmpcol=-1 }
  }

  if (exists $segbound2{$x}) {
  #print STDERR "$x exists!\n";
  my $tmpcol=-1;
  my @tmpcol=("green","magenta");
  for my $xxx (0..$#{$segbound2{$x}}) {
  $tmpcol++;
  my $cury=int ${$segbound2{$x}}[$xxx][0]/$max*$hei;
  my $cury2=int ${$segbound2{$x}}[$xxx][1]/$max*$hei;
  my $yh=$cury2-$cury+1;
  $svg->rectangle(
   x=>$xloc-$wid/2+$wid/6-5,
   y=>$yloc+$cury,
   width=>$wid/4,
   height=>$yh,
   style=> {
    'fill' => $tmpcol[$tmpcol],
    'stroke'         => "white",
    'fill-opacity' => 0.5,
   }
  );
  if ($tmpcol==1) { $tmpcol=-1 }
  }
 }

 }
 
 my $maxsp=0;
 for my $y (keys %{$b{$x}}) {
  my $sp=int $b{$x}{$y}{2}/$max*$hei;
  if ($sp>$maxsp) { $maxsp=$sp }
 }
 $svg->rectangle(
   x=>$xloc,
   y=>$yloc,
   width=>$wid,
   height=>$maxsp,
   style=> {
    'fill' => "rgb(225,225,225)",
    'stroke'         => "rgb(225,225,225)",
   }
  );


 for my $y (keys %{$b{$x}}) {
  my $st=int $b{$x}{$y}{1}/$max*$hei;
  $st++;
  my $sp=int $b{$x}{$y}{2}/$max*$hei;
  #print STDERR " max=$max $b{$x}{$y}{1} st=$st sp=$sp\n";
  my $cw=$wid;
  my $curx=$xloc+($wid-$cw)/2;
  my $cury=$yloc+$st;
  my $curw=$cw;
  my $curh=($sp-$st+2);

  my $col="";
  if ($colbool eq "") {
  $colid++;
  if ($colid==1) { #$colid++ 
   }
  my @cola=@{$s2[$colid]};
  $cola[0]=int(255*$cola[0]);
  $cola[1]=int(255*$cola[1]);
  $cola[2]=int(255*$cola[2]);
  $col=join ",", @cola;
  print STDERR " $x -> $col\n";
  $SEGCOL{$x}{$y}=$col; 
  } else {
   $col="225,225,225";
  }
  #print STDERR " seg: $y st=$st sp=$sp cw=$cw col=$col\n";
  @{$HITSEGPOS{$x}{$y}}=($curx,$cury,$curw,$curh);
  $svg->rectangle(
   x=>$curx,
   y=>$cury,
   width=>$curw,
   height=>$curh,
   style=> {
    'fill' => "rgb($col)",
    'stroke'         => "rgb($col)",
   }
  );
 }
 $xloc+=$wid+$spacer;
}
}



