#!/usr/bin/perl -w
use strict;

open(I,"<$ARGV[0]");
my %seg=();
while (<I>) {
	 chomp;
	 my @tmp = split /\t/;
	 if ($tmp[6] eq "NOCLG") { next }
	 $seg{$tmp[0]}{$tmp[6]}++;
}
close I;

for my $x (keys %seg) {
	print "$x";
 for my $y (keys %{$seg{$x}}) {
  	 print "\t$y-$seg{$x}{$y}";
 }
 print "\n";
}
