## Scripts to generate basic dotplots and chromosomal 'paintings' using amphioxus data


### Data directory
*.msynt - gene location files with gene family ids assigned (for dotplot script)  
bfl-centered.outgroup2.mbh.clus - amphioxus centered mutual best hit ortholog groups  
mbh.clus.para.cons - extended (paralogs added) mutual best hit ortholog groups  
*txt.filt* - gene orthology files  

### Scripts directory
Dotplot script:  
macrosynt.R  
(execute in R)  

### Painting script:  
Example pipeline for CLG contribution plot:  
```bash
#label by CLG assignment
perl mapNodes3.pl ../data/deut.b.clusters.txt.filt.gar . ../data/gar.all.msynt >  gar.all.msynt.col

#partition chromosome into 20-gene windows
perl partition_Complete.pl gar.all.msynt.col 20 > gar.all.msynt.part

#sum up CLG assignments per window
perl sumCol.pl gar.all.msynt.part > gar.dat

perl mapNodes3.pl ../data/deut.b.clusters.txt.filt.chicken . ../data/chicken.all.msynt > chicken.all.msynt.col
perl partition_Complete.pl chicken.all.msynt.col 20 > chicken.all.msynt.part
perl sumCol.pl chicken.all.msynt.part > chicken.dat

perl mapNodes3.pl ../data/deut.b.clusters.txt.filt.frog . ../data/frog.all.msynt >  frog.all.msynt.col
perl partition_Complete.pl frog.all.msynt.col 20 > frog.all.msynt.part
perl sumCol.pl frog.all.msynt.part > frog.dat

perl mapNodes3.pl ../data/deut.b.clusters.txt.filt.human . ../data/human.all.msynt >  human.all.msynt.col
perl partition_Complete.pl human.all.msynt.col 20 > human.all.msynt.part
perl sumCol.pl human.all.msynt.part > human.dat
```

Make image (perl drawCLGContrib.pl SPECIES1 SPECIES2 etc):  
```bash
perl drawCLGContrib.pl ../data/CLG.bfl-cent.mbh.msynt,,,, gar.all.msynt.part,gar.dat,'LG',, chicken.all.msynt.part,chicken.dat,'[^JHLKA]*$',, frog.all.msynt.part,frog.dat,'[^s]',, human.all.msynt.part,human.dat,'[^CK]',,  > CLG_contributions.svg
```
