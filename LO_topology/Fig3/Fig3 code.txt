This is a markdown file explaining all the scripts used for the iScience 2022 paper

This folder contains all python and R scripts used in the analysis. It will be made available once the paper is accepted. 
To run the R scripts, make sure you change the file paths according to your directories.

Scripts are noted in chronological order of the figures on the paper.

- Fig.3 

	heatmap dev script :

`library(tidyverse)
library(magrittr)
library(pheatmap)
getwd()
list.files(“../")

total_tpm_table_TADS <- read.table("../Hannah_TPM_normalizedcounts.txt",
                             header = T,
                             row.names = 1)

mygenes_TADS <- c( "cluster_22486", "cluster_22487", “DAC”…)
names(mygenes_TADS) <- c("cluster_22486", "cluster_22487", “cluster_1342”…)

logical_vector_TADS <- row.names(total_tpm_table_TADS) %in% names(mygenes_TADS)
mygenes_tpm_table_vector_TADS <- total_tpm_table_TADS[logical_vector_TADS,]
rownames(mygenes_tpm_table_vector_TADS) <- mygenes_TADS[rownames(mygenes_tpm_table_vector_TADS)]
mygenes_tpm_table_norm_TADS <- t(scale(t(mygenes_tpm_table_vector_TADS)))
pheatmap::pheatmap(mygenes_tpm_table_norm_TADS, cluster_cols = F)
colnames(mygenes_tpm_table_vector_TADS)
tpm_to_tibble_TADS <- readr::read_delim("../Hannah_TPM_normalizedcounts.txt", delim = " ")

total_tpm_df_TADS <- tpm_to_tibble_TADS %>% 
 dplyr::transmute(gene_id =gene_id,
                  
                  st14 = rowMeans(dplyr::select(., st14_1, st14_2, st14_3)),
                  st16 = rowMeans(dplyr::select(., st16_1, st16_2, st16_3)),
                  st18 = rowMeans(dplyr::select(., st18_1, st18_2, st18_3)),
                  st20 = rowMeans(dplyr::select(., st20_1, st20_2, st20_3)),
                  st22 = rowMeans(dplyr::select(., st22_1, st22_2, st22_3)),
                  st24 = rowMeans(dplyr::select(., st24_1, st24_2, st24_3)),
                  st27 = rowMeans(dplyr::select(., st27_1, st27_2, st27_3)),
                  st28 = rowMeans(dplyr::select(., st28_1, st28_2, st28_3))
                  ) %>%
 as.data.frame()

clust_names_TADS <- total_tpm_df_TADS$gene_id 
total_tpm_df_TADS <- total_tpm_df_TADS[-1]
row.names(total_tpm_df_TADS) <- clust_names_TADS

logical_vector_TADS <- row.names(total_tpm_df_TADS) %in% names(mygenes_TADS)
mygenes_tpm_df_TADS <- total_tpm_df_TADS[logical_vector_TADS,]

rownames(mygenes_tpm_df_TADS) <- mygenes_dac[rownames(mygenes_tpm_df_TADS)]

mygenes_tpm_norm_df_TADS <- t(scale(t(mygenes_tpm_df_TADS)))
pheatmap::pheatmap(mygenes_tpm_norm_df_TADS, cluster_cols = F)

	DAC Hic-Map + insulation score:

python /scratch/molevo/Simakov/INSTALL/HiCPlotter/HiCPlotter.py -f /scratch/molevo/Lisar42/hicplotter/Lisa/iced/40000/Sample1_40000_iced.matrix -o Lachesis_group2 -r 40000 -tri 1 -bed /scratch/molevo/Lisar42/hicplotter/Lisa/raw/40000/Sample1_40000_abs.bed -n Lachesis_group2 -chr Lachesis_group2__55_contigs__length_182973478 -ptr 1 -hmc 5 -sn 1 -pi 1 -w 20 -mm 5 -ext pdf -g dac_3_sorted.bed -s 2450 -e 2650

	DAC clusters heatmap: 

mygenes_dac <- c( "cluster_22486", "cluster_22487", "cluster_10158", "cluster_22488", "cluster_22489","cluster_22490", "cluster_12929", "cluster_22491","cluster_8739", "cluster_22492", "HAND2", "cluster_22493", "cluster_22494", "cluster_2879", "cluster_2284", "cluster_14056", "cluster_3399", "cluster_15519", "cluster_7029", "cluster_29084", "cluster_22495", "DAC", "cluster_22485", "cluster_22484", "cluster_1959", "cluster_22483", "cluster_12764", "cluster_5165", "cluster_5164", "cluster_22482", "cluster_22481", "cluster_13794", "cluster_22480", "cluster_22479", "cluster_9217", "cluster_20886", "cluster_8070", "cluster_20885", "cluster_20884", "cluster_20883", "cluster_5080", "GLYCO", "cluster_5670", "cluster_2392", "cluster_24381", "cluster_760" )

names(mygenes_dac) <- c("cluster_22486", "cluster_22487", "cluster_10158", "cluster_22488", "cluster_22489","cluster_22490", "cluster_12929", "cluster_22491","cluster_8739", "cluster_22492", "cluster_1846", "cluster_22493", "cluster_22494", "cluster_2879", "cluster_2284", "cluster_14056", "cluster_3399", "cluster_15519", "cluster_7029", "cluster_29084", "cluster_22495", "cluster_1342", "cluster_22485", "cluster_22484", "cluster_1959", "cluster_22483", "cluster_12764", "cluster_5165", "cluster_5164", "cluster_22482", "cluster_22481", "cluster_13794", "cluster_22480", "cluster_22479", "cluster_9217", "cluster_20886", "cluster_8070", "cluster_20885", "cluster_20884", "cluster_20883", "cluster_5080", "cluster_6766", "cluster_5670", "cluster_2392", "cluster_24381", "cluster_760")
	
	