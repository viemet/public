#all figures: FIGURESTOPLOT=c("1c_1","1c_2","1c_3","2a","2b","2c","sup1","sup2","sup3")
FIGURESTOPLOT=c("1c_1","1c_2","1c_3","2a","2b","2c","sup1","sup2","sup3","sup4")

##GLOBAL options
FOLDER="./DATA/"
OUTFOLDER="./plots/"
PREFIX=''

#show only scaffolds/chromosomes that have shared gene families
showonlyshared=1

#max paralogs, if available
lim=4
#min genes per scaffold
nmin=10
#max number of scaffolds to plot
maxscf=1000000

#plot bp coordinate, instead of gene index?
REALBP=0

#misc
cex=2
#axis label size
cex_axis=0.5

##END GLOBAL options

if ("1c_1" %in% FIGURESTOPLOT) {

SUFF='.shared.mbh.psynt'
PREFIX="Fig1c_1."
#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe02","Dpe16","Dpe41","Dpe30","Dpe40","Dpe23","Dpe29","Dpe25","Dpe08","Dpe09","Dpe32","Dpe36","Dpe01","Dpe33","Dpe45","Dpe05","Dpe22","Dpe39","Dpe27","Dpe31","Dpe35","Dpe37","Dpe07","Dpe24","Dpe34","Dpe44","Dpe20","Dpe26","Dpe10","Dpe13","Dpe38","Dpe14","Dpe19","Dpe17","Dpe28","Dpe18","Dpe06","Dpe12","Dpe15","Dpe46","Dpe21","Dpe42","Dpe04","Dpe43","Dpe03","Dpe11")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='vampyre'
NAMEB='Vampyroteuthis'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.5 #0.5
lineb_lty=2   #2

#reverse axes?
REV=1

#region to zoom in
realbploc2=NULL

# Change this to get a different p-value cutoff!
COLORSIG=0.05 #default 0.05
gray="#A0A0A0"
# dot size
inches=1/50 #1/50 default, 1/10 large

source("psyntPlot.R")
}




if ("1c_2" %in% FIGURESTOPLOT) {

SUFF='.shared.mbh.psynt'
PREFIX="Fig1c_2."
#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe02","Dpe16","Dpe41","Dpe30","Dpe40","Dpe23","Dpe29","Dpe25","Dpe08","Dpe09","Dpe32","Dpe36","Dpe01","Dpe33","Dpe45","Dpe05","Dpe22","Dpe39","Dpe27","Dpe31","Dpe35","Dpe37","Dpe07","Dpe24","Dpe34","Dpe44","Dpe20","Dpe26","Dpe10","Dpe13","Dpe38","Dpe14","Dpe19","Dpe17","Dpe28","Dpe18","Dpe06","Dpe12","Dpe15","Dpe46","Dpe21","Dpe42","Dpe04","Dpe43","Dpe03","Dpe11")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='hians'
NAMEB='Argonauta hians'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.5 #0.5
lineb_lty=2   #2

#reverse axes?
REV=1

#region to zoom in
realbploc2=NULL

# Change this to get a different p-value cutoff!
COLORSIG=0.05 #default 0.05
gray="#A0A0A0"
# dot size
inches=1/50 #1/50 default, 1/10 large

source("psyntPlot.R")
}



if ("1c_3" %in% FIGURESTOPLOT) {

SUFF='.shared.mbh.psynt'
PREFIX="Fig1c_3."
#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe02","Dpe16","Dpe41","Dpe30","Dpe40","Dpe23","Dpe29","Dpe25","Dpe08","Dpe09","Dpe32","Dpe36","Dpe01","Dpe33","Dpe45","Dpe05","Dpe22","Dpe39","Dpe27","Dpe31","Dpe35","Dpe37","Dpe07","Dpe24","Dpe34","Dpe44","Dpe20","Dpe26","Dpe10","Dpe13","Dpe38","Dpe14","Dpe19","Dpe17","Dpe28","Dpe18","Dpe06","Dpe12","Dpe15","Dpe46","Dpe21","Dpe42","Dpe04","Dpe43","Dpe03","Dpe11")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='OctVul'
NAMEB='Octopus vulgaris'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=1
lineb_lty=1

#reverse axes?
REV=1

#region to zoom in
realbploc2=NULL

# Change this to get a different p-value cutoff!
COLORSIG=0.05 #default 0.05
gray="#A0A0A0"
# dot size
inches=1/50 #1/50 default, 1/10 large

source("psyntPlot.R")
}



if ("2a" %in% FIGURESTOPLOT) {

SUFF='.mbh.psynt'
PREFIX="Fig2a."
# Change this to get a different p-value cutoff!
COLORSIG=1
gray="#A0A0A0"
# dot size
inches=1/10 
#region to zoom in
realbploc2=NULL

#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe03")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='vampyre'
NAMEB='Vampyroteuthis'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=0
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.5 
lineb_lty=2 

#reverse axes?
REV=1

source("psyntPlot.R")

#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe03")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='hians'
NAMEB='Argonauta hians'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=0
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.5 
lineb_lty=2 

#reverse axes?
REV=1


source("psyntPlot.R")



#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe03")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='OctVul'
NAMEB='Octopus vulgaris'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=0
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.5 
lineb_lty=2 

#reverse axes?
REV=1

source("psyntPlot.R")

}




if ("2b" %in% FIGURESTOPLOT) {

SUFF='.mbh.psynt'
PREFIX="Fig2b."
# Change this to get a different p-value cutoff!
COLORSIG=1
gray="#A0A0A0"
# dot size
inches=1/10 
#region to zoom in
realbploc2=NULL

#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe35","Dpe37")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='vampyre'
NAMEB='Vampyroteuthis'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=0
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.5 
lineb_lty=2 

#reverse axes?
REV=1

source("psyntPlot.R")


#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe35","Dpe37")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='hians'
NAMEB='Argonauta hians'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=0
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.5 
lineb_lty=2 

#reverse axes?
REV=1


source("psyntPlot.R")



#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe35","Dpe37")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='OctVul'
NAMEB='Octopus vulgaris'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=0
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=1
lineb_lty=1 

#reverse axes?
REV=1

source("psyntPlot.R")

}




if ("2c" %in% FIGURESTOPLOT) {

SUFF='.mbh.psynt'
PREFIX="Fig2c."
# Change this to get a different p-value cutoff!
COLORSIG=1
gray="#A0A0A0"
# dot size
inches=1/10 
#region to zoom in
realbploc2=NULL

#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe23","Dpe29","Dpe25","Dpe08","Dpe09")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=2
linea_lty=1

#SPECIES B - y axis
FILEB='vampyre'
NAMEB='Vampyroteuthis'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=0
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=1 
lineb_lty=2 

#reverse axes?
REV=1

source("psyntPlot.R")


#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe23","Dpe29","Dpe25","Dpe08","Dpe09")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=2
linea_lty=1

#SPECIES B - y axis
FILEB='hians'
NAMEB='Argonauta hians'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=0
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=1
lineb_lty=2 

#reverse axes?
REV=1


source("psyntPlot.R")



#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe23","Dpe29","Dpe25","Dpe08","Dpe09")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=2
linea_lty=1

#SPECIES B - y axis
FILEB='OctVul'
NAMEB='Octopus vulgaris'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=0
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=2
lineb_lty=1 

#reverse axes?
REV=1

source("psyntPlot.R")

}


if ("sup1" %in% FIGURESTOPLOT) {
 
SUFF='.mbh.psynt'
PREFIX="SupFig1."
# Change this to get a different p-value cutoff!
COLORSIG=0.05
gray="#A0A0A0"
# dot size
inches=1/50 
#region to zoom in
realbploc2=NULL

cex_axis=1

#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=0       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=NULL
#order species A, T for order, NA for skip
order_spa=T
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='npo'
NAMEB='Nautilus pompilius'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=0
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=1 
lineb_lty=1

#reverse axes?
REV=1

source("psyntPlot.R")




 
SUFF='.mbh.psynt'
# Change this to get a different p-value cutoff!
COLORSIG=0.05
gray="#A0A0A0"
# dot size
inches=1/50 
#region to zoom in
realbploc2=NULL

cex_axis=1

#SPECIES A - x axis
FILEA='npo'
NAMEA='Nautilus pompilius'
selclgbool1=0       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=NULL
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='OctVul'
NAMEB='Octopus vulgaris'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=0
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=1 
lineb_lty=1

#reverse axes?
REV=0

source("psyntPlot.R")




}




if ("sup2" %in% FIGURESTOPLOT) {

SUFF='.mbh.psynt'
PREFIX="SupFig2."
# Change this to get a different p-value cutoff!
COLORSIG=0.05
gray="#A0A0A0"
# dot size
inches=1/50 
#region to zoom in
realbploc2=NULL

cex_axis=1

#SPECIES A - x axis
FILEA='npo'
NAMEA='Nautilus pompilius'
selclgbool1=0       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=NULL
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='vampyre'
NAMEB='Vampyroteuthis infernalis'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.25 
lineb_lty=2   

#reverse axes?
REV=0

source("psyntPlot.R")



#SPECIES A - x axis
FILEA='eup.chrname'
NAMEA='Euprymna scolopes'
selclgbool1=0       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=NULL
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='vampyre'
NAMEB='Vampyroteuthis infernalis'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.25 
lineb_lty=2  

#reverse axes?
REV=0

source("psyntPlot.R")




#SPECIES A - x axis
SUFF='.shared.mbh.psynt'
FILEA='ThyRhom'
NAMEA='Thysanoteuthis rhombus'
selclgbool1=0       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=NULL
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='vampyre'
NAMEB='Vampyroteuthis infernalis'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.25
lineb_lty=2

#reverse axes?
REV=0

source("psyntPlot.R")


#SPECIES A - x axis
FILEA='sepia_lycidas'
NAMEA='Sepia lycidas'
selclgbool1=0       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=NULL
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='vampyre'
NAMEB='Vampyroteuthis infernalis'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.25
lineb_lty=2

#reverse axes?
REV=0

source("psyntPlot.R")






#SPECIES A - x axis
FILEA='hians'
NAMEA='Argonauta hians'
selclgbool1=0       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=NULL
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=0.5 
linea_lty=2  

#SPECIES B - y axis
FILEB='vampyre'
NAMEB='Vampyroteuthis infernalis'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.25 
lineb_lty=2  

#reverse axes?
REV=0

source("psyntPlot.R")





#SPECIES A - x axis
SUFF='.mbh.psynt'
FILEA='OctVul'
NAMEA='Octopus vulgaris'
selclgbool1=0       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=NULL
#order species A, T for order, NA for skip
order_spa=NA
lineb_lwd=1 
lineb_lty=1  

#SPECIES B - y axis
FILEB='vampyre'
NAMEB='Vampyroteuthis infernalis'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=0.25 
lineb_lty=2  

#reverse axes?
REV=0

source("psyntPlot.R")





}


if ("sup3" %in% FIGURESTOPLOT) {

SUFF='.mbh.psynt'
PREFIX="SupFig3."
# Change this to get a different p-value cutoff!
COLORSIG=0.05
gray="#A0A0A0"
# dot size
inches=1/50 
#region to zoom in
realbploc2=NULL

cex_axis=1

#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=0       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=NULL
#order species A, T for order, NA for skip
order_spa=T
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='OctVul.part'
NAMEB='Octopus vulgaris, partitioned'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=NA
lineb_lwd=0.25 
lineb_lty=2   

#reverse axes?
REV=1

source("psyntPlot.R")
}


if ("sup4" %in% FIGURESTOPLOT) {

SUFF='.mbh.psynt'
PREFIX="SupFig4."
# Change this to get a different p-value cutoff!
COLORSIG=0.05
gray="#A0A0A0"
# dot size
inches=1/50 
#region to zoom in
realbploc2=NULL

cex_axis=1

#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe23","Dpe29","Dpe25","Dpe08","Dpe09","Dpe35","Dpe37","Dpe02")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='ThyRhom'
NAMEB='Thysanoteuthis rhombus'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=1 
lineb_lty=1

#reverse axes?
REV=1

source("psyntPlot.R")



#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe23","Dpe29","Dpe25","Dpe08","Dpe09","Dpe35","Dpe37","Dpe02")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='eup.chrname'
NAMEB='Euprymna scolopes'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=1 
lineb_lty=1

#reverse axes?
REV=1

source("psyntPlot.R")




#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe23","Dpe29","Dpe25","Dpe08","Dpe09","Dpe35","Dpe37","Dpe02")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='SepLess'
NAMEB='Sepioteuthis lessoniana'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=1 
lineb_lty=1

#reverse axes?
REV=1

source("psyntPlot.R")





#SPECIES A - x axis
FILEA='dpe'
NAMEA='Doryteuthis pealeii'
selclgbool1=1       # set this to 1 if I want to specify contigs (0 if all chromosomes should be plotted)
SELCLG1=c("Dpe23","Dpe29","Dpe25","Dpe08","Dpe09","Dpe35","Dpe37","Dpe02")
#order species A, T for order, NA for skip
order_spa=NA
linea_lwd=1
linea_lty=1

#SPECIES B - y axis
FILEB='sepia_lycidas'
NAMEB='Sepia lycidas'
selclgbool2=0
SELCLG2=NULL
#order by species in A
orderByA=1
#order rows, T for order, NA for skip
order_spb=T
lineb_lwd=1 
lineb_lty=1

#reverse axes?
REV=1

source("psyntPlot.R")

}



