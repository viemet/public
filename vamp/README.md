## Scripts to generate figures

### Data preparation
For genomes without annotation, Doryteuthis proteins were mapped using minimap 2.26. Orthology between protein sequences were computed using BLAST 2.16.0+ . Available tools [Simakov et al, 2020/2022] were used to compute mutual best hits and convert into .psynt files.

### Data directory
./DATA contains .psynt pairwise synteny files

### Producing plots
Rscript plot_figures.R

Figures and debug plots and tables should be produced in the "output" directory

### Miscellaneous
For Figure 1, pairwise MBH/PSYNT files were subset to include the same Doryteuthis gene models:
grep -v "^scDpe" dpe-hians.mbh.psynt | cut -f2 > hians.dpe
cut -f2 dpe-OctVul.mbh.psynt > octvul.dpe
cut -f2 dpe-vampyre.mbh.psynt > vampyre.dpe
grep -f octvul.dpe hians.dpe > octvul.hians.dpe
grep -f vampyre.dpe octvul.hians.dpe > shared.dpe

